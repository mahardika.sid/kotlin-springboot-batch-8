package com.techno.springbootdasar.config

import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.InterceptorRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer

@Configuration
class PathMatchingInterceptor(
    val authInterceptor: AuthInterceptor,
    val requestInterceptor: RequestInterceptor
) : WebMvcConfigurer {
    override fun addInterceptors(registry : InterceptorRegistry){
        registry.addInterceptor(authInterceptor).excludePathPatterns(
            "/v1/api/jwt/encode",
            "/v1/api/account/login",
            "/swagger-ui/**",
            "/v2/api-docs",
            "/swagger-resources/**",
            "/swagger-ui.html",
            "/webjars/**"
        )
        registry.addInterceptor(requestInterceptor).excludePathPatterns(
            "/swagger-ui/**",
            "/v2/api-docs",
            "/swagger-resources/**",
            "/swagger-ui.html",
            "/webjars/**"
        )
    }
}