package com.techno.springbootdasar.config

import com.fasterxml.jackson.databind.ObjectMapper
import com.techno.springbootdasar.domain.common.StatusCode
import com.techno.springbootdasar.domain.dto.response.ResBaseDto
import com.techno.springbootdasar.util.JWTGenerator
import io.jsonwebtoken.ExpiredJwtException
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.servlet.HandlerInterceptor
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class AuthInterceptor(
    @Value("\${header.request.api-key}")
    private val apiKey : String
) : HandlerInterceptor{
    override fun preHandle(request: HttpServletRequest, response: HttpServletResponse, handler: Any): Boolean {
        val apiKeyRequest = request.getHeader("APIKey")
        val token = request.getHeader("token")

        if (token == null || apiKeyRequest == null){
            val body:ResBaseDto<String> = ResBaseDto(
                outStat = StatusCode.FAILED.code,
                outMess = "Permission denied",
                data = null)
            internalServerError(body,response)
            return false
        }

        if (apiKeyRequest!=apiKey){
            val body:ResBaseDto<String> = ResBaseDto(
                outStat = StatusCode.FAILED.code,
                outMess = "Permission denied",
                data = null)
            internalServerError(body,response)
            return false
        }

        val idUser : String

        try {
            idUser = JWTGenerator().decodeJWT(token).id ?: throw RuntimeException("Invalid Token")
            println("id_user : $idUser")
        }catch (e:ExpiredJwtException){
            e.printStackTrace()
            val body : ResBaseDto<String> = ResBaseDto(StatusCode.FAILED.code, "Invalid Token", null)
            internalServerError(body, response)
            return false
        }

        request.setAttribute("idUser",idUser)
        return super.preHandle(request, response, handler)
    }

    fun internalServerError(body: ResBaseDto<String>, response: HttpServletResponse): HttpServletResponse {
        response.status = HttpStatus.FORBIDDEN.value()
        response.contentType = "application/json"
        response.writer.write(convertObjectToJson(body))

        return response
    }

    fun convertObjectToJson(dto : ResBaseDto<String>): String {
        return ObjectMapper().writeValueAsString(dto)
    }
}