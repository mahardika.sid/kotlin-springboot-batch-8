package com.techno.springbootdasar.repository

import com.techno.springbootdasar.domain.entity.UserEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.transaction.annotation.Transactional
import java.util.UUID

interface UserRepository : JpaRepository<UserEntity, String>{

    // Query by method
    fun findByIdUser(idUser : UUID?) : UserEntity?
    fun findUserEntityByEmailAndPassword(email: String, password: String) : UserEntity?

    @Query("SELECT a FROM UserEntity a WHERE idUser = :idUser")
    fun getByIdUser(idUser: UUID) : UserEntity?

    // query raw native
    @Query("SELECT cast(uuid as varchar), first_name, last_name, age, email, mu.\"role\" FROM mst_user mu WHERE cast(mu.uuid as varchar) = :idUser", nativeQuery = true)
    fun getByIdUserNative(idUser: String) : Collection<List<String>>

    // kusus kalau ada delete / update
    @Modifying
    @Transactional
    fun deleteByIdUser(idUser: UUID) : Int?
}