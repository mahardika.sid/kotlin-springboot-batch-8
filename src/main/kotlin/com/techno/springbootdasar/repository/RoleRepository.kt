package com.techno.springbootdasar.repository

import com.techno.springbootdasar.domain.entity.RoleEntity
import org.springframework.data.jpa.repository.JpaRepository
import java.util.UUID

interface RoleRepository : JpaRepository<RoleEntity,String> {
    fun findByIdRole(idRole : UUID?) : RoleEntity?
}