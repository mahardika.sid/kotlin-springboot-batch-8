package com.techno.springbootdasar.domain.entity

import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.UpdateTimestamp
import java.io.Serializable
import java.time.LocalDateTime
import java.util.UUID
import javax.persistence.*

@Entity
@Table(name = "mst_role")
data class RoleEntity(

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @field:Column(name = "id_role", columnDefinition = "uuid")
    val idRole : UUID? = null,

    @field:Column(name = "name", columnDefinition = "varchar")
    val name : String? = null,

    @CreationTimestamp
    @field:Column(name = "dt_added", updatable = false, columnDefinition = "timestamp")
    val dtAdded : LocalDateTime? = null,

    @UpdateTimestamp
    @field:Column(name = "dt_updated", columnDefinition = "timestamp")
    val dtUpdated : LocalDateTime? = null,

    @field:Column(name = "user_added", updatable = false, columnDefinition = "uuid")
    val userAdded : UUID? = null,

    @field:Column(name = "user_updated", columnDefinition = "uuid")
    val userUpdated : UUID? = null,
) : Serializable
