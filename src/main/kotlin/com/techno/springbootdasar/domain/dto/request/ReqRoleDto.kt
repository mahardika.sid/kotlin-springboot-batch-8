package com.techno.springbootdasar.domain.dto.request

data class ReqRoleDto(
    val name : String? = null
)
