package com.techno.springbootdasar.domain.dto.response

data class ResDecodeJwtDto(
    val id : Int = 0,
    val email : String = ""
)
