package com.techno.springbootdasar.domain.dto.request

import javax.validation.constraints.*

data class ReqDataDto(
    @field:NotBlank(message = "notNullRequest is Not Blank")
    val notNullRequest: String?,
    @field:NotEmpty(message = "notEmptyRequest is Not Empty")
    val notEmptyRequest: String?,
    @field:NotNull(message = "minMaxRequest is Not Null")
    @field:NotEmpty(message = "minMaxRequest is Not Empty")
    @field:Size(min = 5, max = 10, message = "minMaxRequest between 5 to 10")
    val minMaxRequest: String?,
    @field:Positive(message = "positiveRequest only positive")
    val positiveRequest: Int?,
    @field:PositiveOrZero(message = "positiveOrZeroRequest")
    val positiveOrZeroRequest: Int?,
    @field:Negative(message = "negativeRequest")
    val negativeRequest: Int?,
    @field:NegativeOrZero(message = "negativeOrZeroRequest")
    val negativeOrZeroRequest: Int?,
    @field:NotNull(message = "patternRequest is Not Null")
    @field:NotEmpty(message = "patternRequest is Not Empty")
    @field:Pattern(regexp = "^[a-zA-z]*$", message = "patternRequest invalid patern")
    val patternRequest: String?
)
