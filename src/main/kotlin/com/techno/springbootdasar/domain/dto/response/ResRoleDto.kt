package com.techno.springbootdasar.domain.dto.response

data class ResRoleDto(
    val uuid : String?= null,
    val name : String? = null
)
