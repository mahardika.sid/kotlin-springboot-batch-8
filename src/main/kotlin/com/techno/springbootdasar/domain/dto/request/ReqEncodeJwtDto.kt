package com.techno.springbootdasar.domain.dto.request

data class ReqEncodeJwtDto(
    val id : String = "",
    val email: String = "",
    val password: String = ""
)
