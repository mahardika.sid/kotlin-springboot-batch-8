package com.techno.springbootdasar.domain.dto.request

import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

data class ReqLoginDto(

    @field:NotNull(message = "email must not be null")
    @field:NotBlank(message = "email must not be blank")
    @field:NotEmpty(message = "email must not be empty")
    val email : String? = null,

    @field:NotNull(message = "email must not be null")
    @field:NotBlank(message = "password must not be blank")
    @field:NotEmpty(message = "password must not be empty")
    val password : String? =null
)
