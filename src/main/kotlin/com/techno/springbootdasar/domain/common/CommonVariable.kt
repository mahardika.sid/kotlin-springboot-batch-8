package com.techno.springbootdasar.domain.common

class CommonVariable {
    companion object{
        val SUCCESS_MESSAGE = "Success"
        val FAILED_MESSAGE = "Something went wrong"
        val DATA_NOT_FOUND = "Data not found"

        val ENDPOINT = "/api/v1/blabla"
    }
}