package com.techno.springbootdasar.controller

import com.techno.springbootdasar.domain.dto.request.ReqLoginDto
import com.techno.springbootdasar.domain.dto.request.ReqUserDto
import com.techno.springbootdasar.domain.dto.response.ResBaseDto
import com.techno.springbootdasar.domain.dto.response.ResLoginDto
import com.techno.springbootdasar.domain.dto.response.ResUserDto
import com.techno.springbootdasar.service.UserService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@RequestMapping("/v1/api/account")
class AccountController(
    private val userService: UserService
) {

    @PostMapping("/login")
    fun login(@Valid @RequestBody reqLoginDto: ReqLoginDto): ResponseEntity<ResBaseDto<ResLoginDto>> {
        val response = userService.login(reqLoginDto)
        return ResponseEntity.ok().body(response)
    }

    @GetMapping
    fun getAllUser(): ResponseEntity<ResBaseDto<ArrayList<ResUserDto>>> {
        val response = userService.getUserAll()
        return ResponseEntity.ok().body(response)
    }

    @GetMapping("/getall")
    fun getAllUserToken(): ResponseEntity<ResBaseDto<ArrayList<ResUserDto>>> {
        val response = userService.getUserAll()
        return ResponseEntity.ok().body(response)
    }

    @GetMapping("/{idUser}")
    fun getByUserById(@PathVariable("idUser") idUser : String): ResponseEntity<ResBaseDto<ResUserDto>> {
        val respose = userService.getUserById(idUser)
        return ResponseEntity.ok().body(respose)
    }

    @PostMapping
    fun insertUser(@RequestBody reqUserDto: ReqUserDto): ResponseEntity<ResBaseDto<ResUserDto>> {
        val response = userService.insertUser(reqUserDto)
        return ResponseEntity.ok().body(response)
    }

    @PutMapping("/{idUser}")
    fun updateUser(
        @RequestBody reqUserDto: ReqUserDto,
        @PathVariable("idUser") idUser: String
    ): ResponseEntity<ResBaseDto<ResUserDto>> {
        val response = userService.updateUser(
            reqUserDto,
            idUser
        )
        return ResponseEntity.ok().body(response)
    }

    @DeleteMapping("/{idUser}")
    fun deleteUser(@PathVariable("idUser") idUser: String): ResponseEntity<ResBaseDto<Any>> {
        val response = userService.deleteUser(idUser)
        return ResponseEntity.ok().body(response)
    }
}