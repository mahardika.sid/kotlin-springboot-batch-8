package com.techno.springbootdasar.controller

import com.techno.springbootdasar.domain.dto.request.ReqRoleDto
import com.techno.springbootdasar.domain.dto.response.ResBaseDto
import com.techno.springbootdasar.domain.dto.response.ResRoleDto
import com.techno.springbootdasar.service.RoleService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.servlet.http.HttpServletRequest

@RestController
@RequestMapping("/v1/api/role")
class RoleController(
    private val roleService: RoleService
) {

    @GetMapping
    fun getRole(): ResponseEntity<ResBaseDto<ArrayList<ResRoleDto>>> {
        val response = roleService.getAllRole()
        return ResponseEntity.ok().body(response)
    }

    @PostMapping
    fun insRole(@RequestBody reqRoleDto: ReqRoleDto, httpServletRequest: HttpServletRequest): ResponseEntity<ResBaseDto<ResRoleDto>> {
        val idUser = httpServletRequest.getAttribute("idUser") as String?
        val response = roleService.insertRole(reqRoleDto,idUser)
        return ResponseEntity.ok().body(response)
    }
}