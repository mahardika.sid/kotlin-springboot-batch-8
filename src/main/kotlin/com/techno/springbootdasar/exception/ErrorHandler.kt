package com.techno.springbootdasar.exception

import com.techno.springbootdasar.domain.common.CommonVariable
import com.techno.springbootdasar.domain.common.StatusCode
import com.techno.springbootdasar.domain.dto.response.ResBaseDto
import org.springframework.core.Ordered
import org.springframework.core.annotation.Order
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import java.util.LinkedHashMap

@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
class ErrorHandler {
    @ExceptionHandler(MethodArgumentNotValidException::class)
    fun handleArgumentNotValidException(exception: MethodArgumentNotValidException): ResponseEntity<ResBaseDto<MutableList<String>>> {
        val errors = mutableListOf<String>()
        exception.bindingResult.fieldErrors.forEach{
            errors.add(it.defaultMessage!!)
        }
        val result = ResBaseDto(
            outStat = StatusCode.FAILED.code,
            outMess = CommonVariable.FAILED_MESSAGE,
            data = errors
        )
        return ResponseEntity.badRequest().body(result)
    }

    @ExceptionHandler(RuntimeException::class)
    fun handleCustomException(exception: RuntimeException): ResponseEntity<Any>{
        exception.printStackTrace()
        val message : LinkedHashMap<String, String?> = LinkedHashMap()
        message["ERROR"] = exception.message
        val result = ResBaseDto(
            outStat = StatusCode.FAILED.code,
            outMess = CommonVariable.FAILED_MESSAGE,
            data = message
        )
        return ResponseEntity.badRequest().body(result)
    }


}