package com.techno.springbootdasar.service

import com.techno.springbootdasar.domain.dto.request.ReqRoleDto
import com.techno.springbootdasar.domain.dto.response.ResBaseDto
import com.techno.springbootdasar.domain.dto.response.ResRoleDto

interface RoleService {
    fun getAllRole() : ResBaseDto<ArrayList<ResRoleDto>>
    fun insertRole(reqRoleDto: ReqRoleDto, idUser : String?) : ResBaseDto<ResRoleDto>
}