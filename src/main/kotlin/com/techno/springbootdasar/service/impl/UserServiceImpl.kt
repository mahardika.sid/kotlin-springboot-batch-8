package com.techno.springbootdasar.service.impl

import com.techno.springbootdasar.domain.common.CommonVariable
import com.techno.springbootdasar.domain.common.StatusCode
import com.techno.springbootdasar.domain.dto.request.ReqLoginDto
import com.techno.springbootdasar.domain.dto.request.ReqUserDto
import com.techno.springbootdasar.domain.dto.response.ResBaseDto
import com.techno.springbootdasar.domain.dto.response.ResLoginDto
import com.techno.springbootdasar.domain.dto.response.ResUserDto
import com.techno.springbootdasar.domain.entity.UserEntity
import com.techno.springbootdasar.repository.RoleRepository
import com.techno.springbootdasar.repository.UserRepository
import com.techno.springbootdasar.service.UserService
import com.techno.springbootdasar.util.JWTGenerator
import org.springframework.stereotype.Service
import java.util.*
import kotlin.collections.ArrayList

@Service
class UserServiceImpl(
    private val userRepository: UserRepository,
    private val roleRepository: RoleRepository
) : UserService {
    override fun login(reqLoginDto: ReqLoginDto): ResBaseDto<ResLoginDto> {
        val data = userRepository.findUserEntityByEmailAndPassword(reqLoginDto.email!!, reqLoginDto.password!!) ?: throw RuntimeException("User not found")
        val token = JWTGenerator().createJWT(data.idUser.toString(), data.email!!)
        val response = ResLoginDto(
            uuid = data.idUser.toString(),
            firstName = data.firstName,
            lastName = data.lastName,
            role = data.idRole?.name,
            age = data.age,
            email = data.email,
            token = token
        )
        return ResBaseDto(data = response)
    }

    override fun getUserAll(): ResBaseDto<ArrayList<ResUserDto>> {
        val data = userRepository.findAll()
        if (data.isEmpty()) throw RuntimeException("Data not found")
        val response : ArrayList<ResUserDto> = ArrayList()
        data.forEach {
            response.add(
                ResUserDto(
                    uuid = it.idUser.toString(),
                    firstName = it.firstName,
                    lastName = it.lastName,
                    age = it.age,
                    email = it.email,
                    role = it.idRole?.name
                )
            )
        }
        return ResBaseDto(data = response)
    }

    override fun getUserById(idUser: String): ResBaseDto<ResUserDto> {
        val data = userRepository.getByIdUser(UUID.fromString(idUser))
        val data2 = userRepository.getByIdUserNative(idUser)

        //contoh raw native
        if (data2.isEmpty())
            return ResBaseDto(
                outStat = StatusCode.FAILED.code,
                outMess = "Data not found",
                data = null
            )

        if (data == null)
            return ResBaseDto(
                outStat = StatusCode.FAILED.code,
                outMess = CommonVariable.FAILED_MESSAGE,
                data = null
            )

        val responseNative = ResUserDto(
            uuid = data2.first()[0],
            firstName = data2.first()[1],
            lastName = data2.first()[2],
            age = data2.first()[3].toInt(),
            email = data2.first()[4],
            role = data2.first()[5]
        )

        val response = ResUserDto(
            uuid = data.idUser.toString(),
            firstName = data.firstName,
            lastName = data.lastName,
            age = data.age,
            email = data.email,
            role = data.idRole?.name
        )

        return ResBaseDto(data = responseNative)
    }

    override fun insertUser(reqUserDto: ReqUserDto): ResBaseDto<ResUserDto> {
        val data = UserEntity(
            firstName = reqUserDto.firstName,
            lastName = reqUserDto.lastName,
            age = reqUserDto.age,
            email = reqUserDto.email,
            password = reqUserDto.password,
            role = reqUserDto.role,
            idRole = roleRepository.findByIdRole(UUID.fromString(reqUserDto.idRole))
        )
        val resUser = userRepository.save(data)
        val response = ResUserDto(
            uuid = resUser.idUser.toString(),
            firstName = resUser.firstName,
            lastName = resUser.lastName,
            age = resUser.age,
            email = resUser.email,
            role = resUser.idRole?.name
        )

        return ResBaseDto(data = response)
    }

    override fun updateUser(reqUserDto: ReqUserDto, idUser: String): ResBaseDto<ResUserDto> {
        val data = userRepository.findByIdUser(UUID.fromString(idUser)) ?:
            return ResBaseDto(
                outStat = StatusCode.FAILED.code,
                outMess = "Data not found",
                data = null
            )
        val newData = data.copy(
            firstName = reqUserDto.firstName,
            lastName = reqUserDto.lastName,
            age = reqUserDto.age,
            email = reqUserDto.email,
            password = reqUserDto.password,
            role = reqUserDto.role
        )
        val resUser = userRepository.save(newData)
        val response = ResUserDto(
            uuid = resUser.idUser.toString(),
            firstName = resUser.firstName,
            lastName = resUser.lastName,
            age = resUser.age,
            email = resUser.email,
            role = resUser.role
        )
        return ResBaseDto(data = response)
    }

    override fun deleteUser(idUser: String): ResBaseDto<Any> {
        userRepository.deleteByIdUser(UUID.fromString(idUser)) ?:
            return ResBaseDto(
                outStat = StatusCode.FAILED.code,
                outMess = CommonVariable.FAILED_MESSAGE,
                data = null
            )

        return ResBaseDto(data = null)
    }
}