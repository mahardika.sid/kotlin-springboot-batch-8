package com.techno.springbootdasar.service.impl

import com.techno.springbootdasar.service.LogicService
import org.springframework.stereotype.Service

@Service
class LogicServiceImpl : LogicService{
    override fun oddOrEven(number: Int): String {
        return if (number % 2 == 0)
            "Genap"
        else
            "Ganjil"
    }
}