package com.techno.springbootdasar.service.impl

import com.techno.springbootdasar.domain.dto.request.ReqRoleDto
import com.techno.springbootdasar.domain.dto.response.ResBaseDto
import com.techno.springbootdasar.domain.dto.response.ResRoleDto
import com.techno.springbootdasar.domain.entity.RoleEntity
import com.techno.springbootdasar.repository.RoleRepository
import com.techno.springbootdasar.service.RoleService
import org.springframework.stereotype.Service
import java.util.UUID

@Service
class RoleServiceImpl(
    private val roleRepository: RoleRepository
) : RoleService {
    override fun getAllRole(): ResBaseDto<ArrayList<ResRoleDto>> {
        val data = roleRepository.findAll()
        if (data.isEmpty()) throw RuntimeException("Data not found")
        val response : ArrayList<ResRoleDto> = ArrayList()
        data.forEach {
            response.add(
                ResRoleDto(
                    uuid = it.idRole.toString(),
                    name = it.name
                )
            )
        }
        return ResBaseDto(data = response)
    }

    override fun insertRole(reqRoleDto: ReqRoleDto, idUser: String?): ResBaseDto<ResRoleDto> {
        val data = RoleEntity(
            name = reqRoleDto.name,
            userAdded = UUID.fromString(idUser)
        )
        val resData = roleRepository.save(data)
        val response = ResRoleDto(
            uuid = resData.idRole.toString(),
            name = resData.name
        )
        return ResBaseDto(data = response)
    }
}